import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/courselist',
    name: 'Course List',
    component: () => import('../views/Course/Courselist.vue')
  },
  {
    path: '/formInput1',
    name: 'Form Input 1',
    component: () => import('../views/FormInput1.vue')
  },
  {
    path: '/formInput2',
    name: 'Form Input 2',
    component: () => import('../views/Course Structure/FormInput2.vue')
  },
  {
    path: '/formInput3',
    name: 'Form Input 3',
    component: () => import('../views/Subject Type/FormInput3.vue')
  },
  {
    path: '/formInput4',
    name: 'Form Input 4',
    component: () => import('../views/Module Subject/FormInput4.vue')
  },
  {
    path: '/formStudent',
    name: 'Form Student',
    component: () => import('../views/Student/FormStudent.vue')
  },
  {
    path: '/subject',
    name: 'Subject',
    component: () => import('../views/Subject/Subject.vue')
  },
  {
    path: '/study',
    name: 'Study',
    component: () => import('../views/Ui student/StudyResult/Study.vue')
  },
  {
    path: '/subjectstudent',
    name: 'Substudent',
    component: () => import('../views/Ui student/Subject Student/Subjectstudent.vue')
  },
  {
    path: '/CheckCourse',
    name: 'CheckCheck',
    component: () => import('../views/Ui student/check/checkcourse.vue')
  },
  {
    path: '/sahagitclass',
    name: 'SaHaGit',
    component: () => import('../views/Ui student/Sahagit/ClassSahagit.vue')
  },
  {
    path: '/signinstudent',
    name: 'Sign',
    component: () => import('../views/Ui student/SigninStu/signin.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
